#!/bin/sh

export DOMAIN=${DOMAIN:-$(hostname --domain)}
export DEBUG=${DEBUG}
#export EMAIL=${EMAIL}
EMAIL=tharangar@opensource.lk
SITE=coppermail.dyndns.org
DEBUG=true
export DOMAIN=${DOMAIN}
# creating certificate files for openldap server


chmod -R 755 /etc/openldap/certs/
export KEY_PATH=/etc/openldap/certs/
files=$(shopt -s nullglob dotglob; echo $KEY_PATH)
echo $KEY_PATH
echo "Checking for existing certificates"

cd  /etc/openldap/certs/

# https://www.itzgeek.com/how-tos/linux/centos-how-tos/configure-openldap-with-ssl-on-centos-7-rhel-7.html
if [ "$DEBUG" = true ]; then
   mkdir -p $KEY_PATH
   #openssl req -nodes -x509 -newkey rsa:4096 -keyout ${KEY_PATH}.privkey.pem -out ${KEY_PATH}.fullchain.pem -days 365 -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=nextgenmed.dyndns.org"
   #openssl req -x509 -days 365 -sha256 -newkey rsa:2048 -nodes -keyout /etc/ssl/private/$SITE.key -out /etc/ssl/certs/$SITE-cert.pem -subj "/C=LK/ST=Colombo/L=Maradana/O=Company Name/OU=Org/CN=coppermail.dyndns.org"
   
   openssl genrsa -out coppermailrootCA.key 2048

   openssl req -x509 -new -nodes -key coppermailrootCA.key -sha256 -days 1024 -out coppermailrootCA.pem -subj "/C=LK/ST=Colombo/L=Maradana/O=Company Name/OU=Org/CN=coppermail.dyndns.org"


   openssl genrsa -out coppermailldap.key 2048

   openssl req -new -key coppermailldap.key -out coppermailldap.csr -subj "/C=LK/ST=Colombo/L=Maradana/O=Company Name/OU=Org/CN=coppermail.dyndns.org"

   openssl x509 -req -in coppermailldap.csr -CA coppermailrootCA.pem -CAkey coppermailrootCA.key -CAcreateserial -out coppermailldap.crt -days 1460 -sha256


  

   echo "IN DEBUG MODE!!!! - GENERATED SELF SIGNED SSL KEY" ll /etc/openldap/certs/coppermail*
  else
if (( ${#files} )); then
       echo "Found existing keys!!"
   else
       echo "No Certicates Found!!"
       echo "Generating SSL Certificates with LetsEncrypt"
       letsencrypt certonly --standalone -d $SITE --noninteractive --agree-tos --email $EMAIL
       if (( ${#files} )); then
         echo "Certicate generation Successfull"
       else
         echo "Certicate generation failed."
         exit 1
       fi
   fi
  fi

# move to the correct let's encrypt directory
#cd /etc/letsencrypt/live/$SITE

# copy the files related to letsencript
#cp cert.pem /etc/ssl/certs/$SITE.cert.pem
#cp fullchain.pem /etc/ssl/certs/$SITE.fullchain.pem
#cp privkey.pem /etc/ssl/private/$SITE.privkey.pem

# adjust permissions of the private key
#chown :ssl-cert /etc/ssl/private/$SITE.privkey.pem
#chmod 640 /etc/ssl/private/$SITE.privkey.pem
# -----------------------------------------------------
# copy files generated from openssl
#cp /etc/ssl/certs/$SITE-cert.pem /etc/ssl/certs/$SITE.cert.pem
#cp *fullchain.pem /etc/ssl/certs/$SITE.fullchain.pem
#cp /etc/ssl/private/$SITE.key /etc/ssl/private/$SITE.privkey.pem

# adjust permissions of the private key
#chown :ssl-cert /etc/ssl/private/$SITE.key
#chmod 640 /etc/ssl/private/$SITE.key


apt-get -f install
rm -rf /var/cache/debconf/*.dat

# Adding postfix plugin

wget -O postfix.ldif https://raw.githubusercontent.com/68b32/postfix-ldap-schema/master/postfix.ldif
ldapadd -ZZ -x -W -D cn=admin,cn=config -H ldap://172.19.0.21 -f postfix.ldif


# restart slapd to load new certificates
service slapd restart
service apache2 restart



